// importer le fichier operations.js
"use strict";

import { add, multiply } from "./operations.js";
// importer la librairie assert
import { equal } from "assert";

describe("testing", () => {
  it("should be 40 when you add 20 and 20", () => {
    // test ...
    const wantedResult = 40;
    const result = add(20, 20);
    // order matters given result, wanted result
    equal(result, wantedResult);
  });

  it("should get 30 when you multiply 5 by 6", () => {
    const mult = multiply(5, 6);
    equal(mult, 30);
  });
});
